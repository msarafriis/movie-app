# Movie App

Using an API for making a movie list.

Thanks to [![The Movie DB](https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_2-d537fb228cf3ded904ef09b136fe3fec72548ebc1fea3fbbd1ad9e36364db38b.svg)](https://themoviedb.org) for providing the data!

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
