const API_KEY = 'insert your API key here'
const API_URL = `https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=1`
const IMG_PATH = 'https://image.tmdb.org/t/p/w500'
const SEARCH_API = (search) =>
  `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query="${search}"`

const form = document.querySelector('form')
const search = document.getElementById('search')
const main = document.querySelector('main')

// Get initial movies
getMovies(API_URL)

async function getMovies(url) {
  const res = await fetch(url)
  const data = await res.json()
  showMovies(data.results)
}

function showMovies(movies) {
  main.innerHTML = ''
  movies.forEach((movie) => {
    const { title, poster_path, vote_average, overview } = movie
    const appendages = []
    const appendageAdder = (list, parentElement) =>
      list.forEach((childElement) => parentElement.appendChild(childElement))

    const poster = document.createElement('img')
    poster.src = IMG_PATH + poster_path
    appendages.push(poster)

    const infoAppendages = []
    const movieInfo = document.createElement('div')
    const movieTitle = document.createElement('h3')
    const rating = document.createElement('span')

    movieTitle.innerText = title
    infoAppendages.push(movieTitle)

    rating.innerText = vote_average
    const ratingClass = (classAdd) => rating.classList.add(classAdd)
    if (vote_average >= 8) {
      ratingClass('green')
    } else if (vote_average >= 5) {
      ratingClass('yellow')
    } else {
      ratingClass('red')
    }
    infoAppendages.push(rating)

    movieInfo.classList.add('movie-info')
    appendageAdder(infoAppendages, movieInfo)
    appendages.push(movieInfo)

    const overviewAppendages = []
    const overviewEl = document.createElement('div')
    const overviewHeading = document.createElement('h3')
    const overviewText = document.createTextNode(overview)
    overviewHeading.innerText = 'Overview'
    overviewAppendages.push(overviewHeading)

    overviewAppendages.push(overviewText)

    overviewEl.classList.add('overview')
    appendageAdder(overviewAppendages, overviewEl)
    appendages.push(overviewEl)

    const movieEl = document.createElement('div')
    movieEl.classList.add('movie')
    appendageAdder(appendages, movieEl)
    main.appendChild(movieEl)
  })
}

form.addEventListener('submit', (e) => {
  e.preventDefault()

  const searchTerm = search.value
  if (searchTerm && searchTerm !== '') {
    getMovies(SEARCH_API(searchTerm))

    search.value = ''
  } else {
    window.location.reload()
  }
})
